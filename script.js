class Person {
    constructor(name, hp, acc, def, attDc, str, accDc, init) {
        this.name = name;
        this.acc = acc;
        this.def = def;
        this.attDc = attDc;
        this.str = str;
        this.accDc = accDc;
        this.init = init;
        //this provide value for checkup in loop at combat() - Dragon and Warrior inherits from Person
        this.isAlive = true;
        this._hpCurrent = this.maxHp = hp;
    }
    get hp() {
        return this._hpCurrent;
    }
    set hp(_hp) {
        this._hpCurrent = _hp;
        this.isAlive = this._hpCurrent > 0;
    }
}
class Warrior extends Person {
    constructor(name, hp, acc, def, attDc, str, accDc, init) {
        super(name, hp, acc, def, attDc, str, accDc, init);
    }
    hit() {
        return this.acc > rollDice(100);
    }
    ;
    atk(target) {
        if (this.hit()) {
            let attackValue = (this.str + rollDice(this.attDc)) - target.def;
            target.hp -= attackValue;
            console.log(`${this.name} attack ${target.name} for ${attackValue} hp`);
        }
        else {
            console.log(`${this.name} miss`);
        }
    }
    ;
    specialAtk(target) {
        throw new Error("Method not implemented.");
    }
}
class Healer extends Warrior {
    constructor(name, hp, acc, def, attDc, str, accDc, init) {
        super(name, hp, acc, def, attDc, str, accDc, init);
    }
    //custom function we provide array of persons
    heal(target) {
        for (let i = 0; i < target.length; i++) {
            target[i].hp += 30;
        }
    }
}
class Dragon extends Person {
    constructor(name, hp, acc, def, attDc, str, accDc, init) {
        super(name, hp, acc, def, attDc, str, accDc, init);
    }
    hit() {
        return this.acc > rollDice(100);
    }
    ;
    atk(target) {
        if (this.hit()) {
            let attackValue = (this.str + rollDice(this.attDc)) - target.def;
            target.hp -= attackValue;
            console.log(`${this.name} attack ${target.name} for ${attackValue} hp`);
        }
        else {
            console.log(`${this.name} miss`);
        }
    }
    ;
    specialAtk(target) {
        for (let i = 0; i < target.length; i++) {
            this.atk(target[i]);
        }
    }
}
//takes value from initCalc and multiply for random Math value
function rollDice(max) {
    return Math.floor((Math.random() * max) + 1);
}
//provide value to rolldice function
function initCalc() {
    return rollDice(10);
}
function combat() {
    //creation of array for team
    let team;
    //filling out team array
    team = [
        //(name, hp, acc, def, attDc, str, accDc, init)
        new Warrior("War1", 100, 80, 10, 10, 100, 10, 10),
        new Warrior("War2", 100, 90, 10, 10, 100, 10, 10),
        new Warrior("War3", 100, 80, 10, 10, 100, 10, 10),
        new Warrior("War4", 100, 70, 10, 10, 100, 10, 10),
        new Healer("Heal1", 100, 70, 10, 10, 100, 10, 10)
    ];
    let dragon = new Dragon("Dragon", 1000, 90, 10, 100, 30, 10, 10);
    while (true) {
        //counter to keep values from loop - checkup is death team members is equal for team.length 
        let counter = 0;
        //checks if any team member or dragon is alive - attack
        for (let i = 0; i < team.length; i++) {
            if (team[i].isAlive && dragon.isAlive) {
                //if team member is healer - heals
                if (team[i] instanceof Healer) {
                    //projection of team[i] to Healer class
                    team[i].heal(team);
                    console.log(team[i].name + " heals");
                }
                //team attack dragon
                team[i].atk(dragon);
                console.log("dragon hp is: " + dragon.hp);
            }
            else
                counter++;
        }
        //if dead break the loop
        if (counter == team.length)
            break;
        //if dargon is alive perform special attack
        if (dragon.isAlive) {
            dragon.specialAtk(team);
            //if dragon is dead break
        }
        else {
            break;
        }
    }
}
//start the loop in combat function
combat();
