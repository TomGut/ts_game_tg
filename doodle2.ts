interface Wehicle {
    name: string,
    speed: number,
    wheels: number,
    forward(): void,
    stop(): void
}

class Car implements Wehicle {
    constructor(public name: string, public speed: number, public wheels: number, private color: string){
    }
    
    forward(): void{
        document.body.innerHTML = "Car go forward";
    }

    stop(): void{
        document.body.innerHTML = "Car stops";
    }
}

class Porshe extends Car {
    constructor(name: string, speed: number, wheels: number, color: string, public engineType: string) {
        super(name, speed, wheels, color);
        this.engineType = engineType;
    }

    public get engine(): string{
        return this.engineType;
    }

    public set engine(name: string){
        this.engineType = name;
    }
}

let myCar = new Porshe("966", 250, 4, "red", "boxer");
myCar.engine = "diesel";

myCar.forward();
myCar.stop();

document.body.innerHTML = "My car is powered by " + myCar.engineType;
console.log(myCar);