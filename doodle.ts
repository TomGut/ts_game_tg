function greeter(name: string): string{
    return "Hello " + name + " !";
}

var usrname = "Tomek";

class person {
    firstname: string;
    weight: number;
}

var tomek = new person;
tomek.firstname = "Tomek";
tomek.weight = 92;

//document.body.innerHTML = tomek.firstname;

let book:string = "Tytułowy tytuł";
let counter:number = 5;
let summary:string = `Na magazynie mamy ${counter + 1} książek o tytule ${book}`;

//document.body.innerHTML = summary;

let list: Array<Number> = [88, 100, 200];

for(let i in list){
    console.log("klucz: " + i); //klucze
}

for (let i of list){
    console.log("wartość: " + i); //wartości
}

interface TitleValue {
    title: string;
    author?: string;
}

function showTitle(titleObject: TitleValue){
    document.body.innerHTML = titleObject.title;
}

let book2 = {
    title: "Tytułowy tytuł",
    author: "Tomek"
}

class bookClass implements TitleValue {
    title: "nowy tytuł"
}

let book3 = new bookClass;
//book3.title = "tytuł książki z klasy";

// showTitle(book2);
// showTitle(book3);

// interface Animal{
//     name: String;
//     move (steps: number) :void;
// }

// class Dog implements Animal{

//     constructor(_name: string){
//         this.name = _name;
//     }
//     name: String;
//     move (steps: number) :void{
//         document.body.innerHTML = `Dog ${this.name}, moved ${steps} steps`;
//     }
// }

// jeżeli chcemy przekazać funkcje move w konstruktorze to
interface Animal{
    name: String;
    move (): void;
}

class Dog implements Animal{
//public bo z interfejsu, private bo u uzyte spoza interfejsu i w konstruktorze
    constructor(public name: string, private steps: number){
    }
    move (): void{
        document.body.innerHTML = `Dog ${this.name}, moved ${this.steps} steps`;
    }
}

let dog = new Dog("Piesio", 25);
//dog.move();


//tu nie używamy już public i protected - bo rozszerzamy klasę używając dziedziczenia pół, jeżeli chielibyśmy dodać
//dodatkową właściwość w konstruktorze, musielibyśmy już dodać proivate lub public
class Shepard extends Dog{
    constructor(name: string, steps: number){
        super(name, steps);
    }

    private _age: number;

    public get age(): number{
        return this._age;
    }

    public set age(age: number){
        this._age = age;
    }
}

let shepard = new Shepard("kora", 7);
shepard.age(12);
shepard.move();